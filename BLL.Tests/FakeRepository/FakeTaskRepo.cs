using System;
using System.Linq;
using System.Collections.Generic;
using DAL.Domain.Models;
using DAL.Domain.Repositories;
using System.Threading.Tasks;
using BLL.Tests.Seeds;

namespace BLL.Tests.FakeRepository
{
    public class FakeTaskRepo : IRepository<TaskModel>
    {
        private readonly IList<TaskModel> _storage;
        public FakeTaskRepo()
        {
            _storage = new TaskSeeds().Tasks;
        }
        public async Task<TaskModel> Create(TaskModel data) =>
        await Task.Run(() =>
        {
            int id = new Random().Next();
            data.Id = id;
            _storage.Add(data);
            return data;
        });

        public async Task<TaskModel> Delete(int id) =>
        await Task.Run(() =>
        {
            var task = _storage.FirstOrDefault(x => x.Id == id);
            _storage.Remove(task);
            return task;
        });

        public async Task<TaskModel> Read(int id) =>
        await Task.Run(() =>
        {
            return _storage.FirstOrDefault(x => x.Id == id);
        });

        public async Task<IList<TaskModel>> ReadAll() =>
        await Task.Run(() =>
        {
            return _storage;
        });

        public async Task<TaskModel> Update(TaskModel data) =>
        await Task.Run(() =>
        {
            var task = _storage.FirstOrDefault(x => x.Id == data.Id);
            var taskIndex = _storage.IndexOf(task);
            _storage[taskIndex] = data;
            return data;
        });
    }
}