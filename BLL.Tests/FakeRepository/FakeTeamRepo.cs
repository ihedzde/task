using System;
using System.Linq;
using System.Collections.Generic;
using DAL.Domain.Models;
using DAL.Domain.Repositories;
using System.Threading.Tasks;
using BLL.Tests.Seeds;

namespace BLL.Tests.FakeRepository
{
    public class FakeTeamRepo : IRepository<TeamModel>
    {
        private readonly IList<TeamModel> _storage;
        public FakeTeamRepo()
        {
            _storage = new TeamSeeds().Teams;
        }
        public async Task<TeamModel> Create(TeamModel data) =>
        await Task.Run(() =>
        {
            int id = new Random().Next();
            data.Id = id;
            _storage.Add(data);
            return data;
        });

        public async Task<TeamModel> Delete(int id) =>
        await Task.Run(() =>
        {
            var team = _storage.FirstOrDefault(x => x.Id == id);
            _storage.Remove(team);
            return team;
        });

        public async Task<TeamModel> Read(int id) =>
        await Task.Run(() =>
        {
            return _storage.FirstOrDefault(x => x.Id == id);
        });

        public async Task<IList<TeamModel>> ReadAll() =>
          await Task.Run(() =>
          {
              return _storage;
          });

        public async Task<TeamModel> Update(TeamModel data) =>
        await Task.Run(() =>
        {
            var team = _storage.FirstOrDefault(x => x.Id == data.Id);
            var teamIndex = _storage.IndexOf(team);
            _storage[teamIndex] = data;
            return data;
        });
    }
}