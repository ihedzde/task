using System;
using System.Collections.Generic;
using DAL.Domain.Models;


namespace BLL.Tests.Seeds
{
    public class TeamSeeds
    {
        #region Seeds
        public IList<TeamModel> Teams { get; set; } = new List<TeamModel>{
        new TeamModel{
            Id = 1,
            Name = "Durgan Group",
            CreatedAt = DateTime.Parse("2017-03-31T02:29:28.3740504+00:00")
        },
        new TeamModel{
            Id = 2,
            Name = "Kassulke LLC",
            CreatedAt = DateTime.Parse("2019-02-21T15:47:30.3797852+00:00")
        },
        new TeamModel{
            Id = 3,
            Name = "Harris LLC",
            CreatedAt = DateTime.Parse("2018-08-28T08:18:46.4160342+00:00")
        },
        new TeamModel{
            Id = 4,
            Name = "Mitchell Inc",
            CreatedAt = DateTime.Parse("2019-04-03T09:58:33.0178179+00:00")
        },
        new TeamModel{
            Id = 5,
            Name = "Smitham Group",
            CreatedAt = DateTime.Parse("2016-10-05T07:57:02.8427653+00:00")
        },
        new TeamModel{
            Id = 6,
            Name = "Kutch - Roberts",
            CreatedAt = DateTime.Parse("2016-10-31T05:05:15.1076578+00:00")
        },
        new TeamModel{
            Id = 7,
            Name = "Parisian Group",
            CreatedAt = DateTime.Parse("2016-07-17T01:34:55.0917082+00:00")
        },
        new TeamModel{
            Id = 8,
            Name = "Schiller Group",
            CreatedAt = DateTime.Parse("2020-10-05T01:20:14.1953926+00:00")
        },
        new TeamModel{
            Id = 9,
            Name = "Littel, Turcotte and Muller",
            CreatedAt = DateTime.Parse("2018-10-19T14:54:27.5549549+00:00")
        }

                };
        #endregion
    }
}