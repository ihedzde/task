using System;
using System.Linq;
using AutoMapper;
using BLL.Domain.Services;
using BLL.MappingProfiles;
using BLL.Services;
using Xunit;
using FakeItEasy;
using DAL.Domain.Repositories;
using DAL.Domain.Models;
using System.Threading.Tasks;
using BLL.Tests.Seeds;

namespace BLL.Tests
{
    public class AggregateServicesTests
    {
        private readonly AggregateServices _aggregateServices;
        public AggregateServicesTests()
        {
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new UserProfile());
            }).CreateMapper();
            var fakeTeamRepo = A.Fake<IRepository<TeamModel>>();
            A.CallTo(() => fakeTeamRepo.ReadAll()).Returns(Task.FromResult(new TeamSeeds().Teams));
            var fakeUserRepo = A.Fake<IRepository<UserModel>>();
            A.CallTo(() => fakeUserRepo.ReadAll()).Returns(Task.FromResult(new UserSeeds().Users));
            var fakeTaskRepo = A.Fake<IRepository<TaskModel>>();
            A.CallTo(() => fakeTaskRepo.ReadAll()).Returns(Task.FromResult(new TaskSeeds().Tasks));
            var fakeProjectRepo = A.Fake<IRepository<ProjectModel>>();
            A.CallTo(() => fakeProjectRepo.ReadAll()).Returns(Task.FromResult(new ProjectSeeds().Projects));
            ITeamService teamService = new TeamService(mapper, fakeTeamRepo, fakeUserRepo, fakeProjectRepo);
            IUserService userService = new UserService(mapper, fakeUserRepo, fakeTeamRepo);
            ITaskService taskService = new TaskService(mapper, fakeTaskRepo, fakeUserRepo, fakeProjectRepo);
            IProjectService productService = new ProjectService(mapper, fakeProjectRepo, fakeUserRepo, fakeTeamRepo, taskService, teamService);
            _aggregateServices = new AggregateServices(productService, userService);
        }
        [Fact]
        public async Task GetTasksEntitiesById_WhenIdIsNegative_ThanThrowInvalidArgumentException()
        {
            //Arrange
            int id = -10;
            //Act Assert
            await Assert.ThrowsAsync<ArgumentException>(() =>  _aggregateServices.GetTasksEntitiesById(id));
        }
        [Fact]
        public async Task ComplexUserEntity_WhenIdIsNegative_ThanThrowInvalidArgumentException()
        {
            //Arrange
            int id = -110;
            //Act Assert
           await Assert.ThrowsAsync<ArgumentException>(() => _aggregateServices.ComplexUserEntity(id));
        }
        [Fact]
        public async Task GetLessThan45_WhenIdIsNegative_ThanThrowInvalidArgumentException()
        {
            //Arrange
            int id = -34;
            //Act Assert
            await Assert.ThrowsAsync<ArgumentException>(() => _aggregateServices.GetLessThan45(id));
        }
        [Fact]
        public async Task FinishedIn2021_WhenIdIsNegative_ThanThrowInvalidArgumentException()
        {
            //Arrange
            int id = -42;
            //Act Assert
            await Assert.ThrowsAsync<ArgumentException>(() => _aggregateServices.FinishedIn2021(id));
        }
        [Fact]
        public async Task TeamsOlder10Sorted_WhenCalled_ThanReturnsNotEmptyList()
        {

            Assert.NotEmpty(await _aggregateServices.TeamsOlder10Sorted());
        }
        [Fact]
        public async Task UsersAlphabeticByTasksLength_WhenCalled_ThanReturnsNotEmpty()
        {
            Assert.NotEmpty(await _aggregateServices.UsersAlphabeticByTasksLength());
        }
        [Fact]
        public async Task OddlyProjectTaskUserCountMix_WhenCalled_ThanReturnsNotEmpty()
        {
            Assert.NotEmpty(await _aggregateServices.OddlyProjectTaskUserCountMix());
        }
    }
}