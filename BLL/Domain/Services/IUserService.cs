using System.Collections.Generic;
using Common.DTO.User;
using System.Threading.Tasks;
namespace BLL.Domain.Services
{
    public interface IUserService
    {
        Task<IEnumerable<UserDTO>> GetAllUsers();
        Task<UserDTO> GetById(int id);
        Task<UserDTO> CreateUser(CreateUserDTO createUserDTO);
        Task DeleteUser(int id);
        Task UpdateUser(UserDTO updateUser);
    }
}