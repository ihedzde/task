using System.Collections.Generic;
using Common.DTO.Team;
using System.Threading.Tasks;
namespace BLL.Domain.Services
{
    public interface ITeamService
    {
        Task<TeamDTO> CreateTeam(CreateTeamDTO createTeamDTO);
        Task DeleteTeam(int id);
        Task<IEnumerable<TeamDTO>> GetAllTeams();
        Task<TeamDTO> GetById(int id);
        Task UpdateTeam(TeamDTO updateDto);
    }
}