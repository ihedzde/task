using System.Collections.Generic;
using Common.DTO.Task;
using System.Threading.Tasks;
namespace BLL.Domain.Services
{
    public interface ITaskService
    {
        Task <TaskDTO> CreateTask(CreateTaskDTO createTaskDTO);
        Task  DeleteTask(int id);
        Task <IEnumerable<TaskDTO>> GetAllTasks();
        Task <TaskDTO> GetById(int id);
        Task <TaskDTO> UpdateTask(TaskDTO updateDto);
        Task <IEnumerable<TaskDTO>> GetAllUnfinishedForUser(int userId);
    }
}