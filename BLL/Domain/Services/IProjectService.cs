using System.Collections.Generic;
using Common.DTO.Project;
using System.Threading.Tasks;
namespace BLL.Domain.Services
{
    public interface IProjectService
    {
        Task<ProjectDTO> CreateProject(CreateProjectDTO createProjectDTO);
        Task DeleteProject(int id);
        Task<IEnumerable<ProjectDTO>> GetAllProjects();
        Task<ProjectDTO> GetById(int id);
        Task UpdateProject(ProjectDTO updateDto);
    }

}