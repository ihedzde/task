using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLL.Domain.Services;
using Common.DTO.Task;
using Common.DTO.User;
using DAL.Domain.Models;
using DAL.Domain.Repositories;

namespace BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IRepository<TaskModel> _taskRepo;
        private readonly IRepository<UserModel> _userRepo;
        private readonly IRepository<ProjectModel> _projectRepo;
        private readonly IMapper _mapper;
        public TaskService(IMapper mapper, IRepository<TaskModel> taskRepo,
         IRepository<UserModel> userRepo, IRepository<ProjectModel> projectRepo)
        {
            _taskRepo = taskRepo;
            _userRepo = userRepo;
            _projectRepo = projectRepo;
            _mapper = mapper;
        }
        private async Task<TaskDTO> ModelToTaskDTO(TaskModel model)
        {
            var performer = await _userRepo.Read(model.PerformerId);
            return new TaskDTO
            {
                Id = model.Id,
                Name = model.Name,
                PerformerId = model.PerformerId,
                Performer = _mapper.Map<UserModel, UserDTO>(performer),
                ProjectId = model.ProjectId,
                Description = model.TLDR,
                State = (TaskState)model.State,
                FinishedAt = model.FinishedAt,
                CreatedAt = model.CreatedAt
            };
        }
        private TaskModel TaskDTOToModel(TaskDTO model)
        {
            return new TaskModel
            {
                Id = model.Id,
                Name = model.Name,
                ProjectId = model.ProjectId,
                PerformerId = model.PerformerId,
                TLDR = model.Description,
                State = model.State,
                FinishedAt = model.FinishedAt,
                CreatedAt = model.CreatedAt
            };
        }
        private TaskDTO CreateModelToTaskDTO(CreateTaskDTO model) =>
        new TaskDTO
        {
            Name = model.Name,
            PerformerId = model.PerformerId,
            ProjectId = model.ProjectId,
            Description = model.Description,
            State = model.State,
            CreatedAt = DateTime.Now
        };

        public async Task<IEnumerable<TaskDTO>> GetAllTasks()
        {
            var tasks = (await _taskRepo.ReadAll());
            IList<TaskDTO> dtos = new List<TaskDTO>();
            foreach(var model in tasks){
                    TaskDTO dto = await ModelToTaskDTO(model);
                    dtos.Add(dto);
            }
            return dtos;
        }
        private void ValidateCreateDTO(CreateTaskDTO createTaskDTO)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (string.IsNullOrEmpty(createTaskDTO.Name))
                stringBuilder.AppendLine("Name can't be empty");
            if (string.IsNullOrEmpty(createTaskDTO.Description))
                stringBuilder.AppendLine("Description can't be empty");

            if (_projectRepo.Read((int)createTaskDTO.ProjectId) == null)
                stringBuilder.AppendLine($"No project with such {createTaskDTO.ProjectId} projectId was found");
            if (_userRepo.Read((int)createTaskDTO.PerformerId) == null)
                stringBuilder.AppendLine($"No performer with such {createTaskDTO.PerformerId} performerId was found");

            var argumentExceptionMessage = stringBuilder.ToString();
            if (argumentExceptionMessage.Length > 0)
                throw new ArgumentException(argumentExceptionMessage);
        }
        public async Task<TaskDTO> CreateTask(CreateTaskDTO createTaskDTO)
        {
            ValidateCreateDTO(createTaskDTO);
            return await ModelToTaskDTO(await _taskRepo.Create(TaskDTOToModel(CreateModelToTaskDTO(createTaskDTO))));
        }
        public async Task<TaskDTO> GetById(int id)
        {
            return await ModelToTaskDTO(await _taskRepo.Read(id));
        }
        private void ValidateUpdateDTO(TaskDTO updateTaskDTO)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (string.IsNullOrEmpty(updateTaskDTO.Name))
                stringBuilder.AppendLine("Name can't be empty");
            if (string.IsNullOrEmpty(updateTaskDTO.Description))
                stringBuilder.AppendLine("Description can't be empty");

            if (_projectRepo.Read((int)updateTaskDTO.ProjectId) == null)
                stringBuilder.AppendLine($"No project with such {updateTaskDTO.ProjectId} projectId was found");
            if (_userRepo.Read((int)updateTaskDTO.PerformerId) == null)
                stringBuilder.AppendLine($"No performer with such {updateTaskDTO.PerformerId} performerId was found");
            if (updateTaskDTO.CreatedAt > DateTime.Now)
                stringBuilder.AppendLine($"Task can't be created earlier than today{DateTime.Now.ToString()}");
            
            var argumentExceptionMessage = stringBuilder.ToString();
            if (argumentExceptionMessage.Length > 0)
                throw new ArgumentException(argumentExceptionMessage);
        }
        public async Task<TaskDTO> UpdateTask(TaskDTO updateDto)
        {
            ValidateUpdateDTO(updateDto);
            return await ModelToTaskDTO(await _taskRepo.Update(TaskDTOToModel(updateDto)));
        }
        public async Task DeleteTask(int id)
        {
            var model = await _taskRepo.Delete(id);
            if(model == null)
                throw new ArgumentException($"No task with {id} id was found");
        }

        public async Task<IEnumerable<TaskDTO>> GetAllUnfinishedForUser(int userId)
        {
            if(userId < 0)
                throw new ArgumentException("Invalid userId can't be negative");
            var tasks = (await _taskRepo.ReadAll()).Where(task=>task.PerformerId == userId && (TaskState)task.State != TaskState.Finished);
            IList<TaskDTO> dtos = new List<TaskDTO>();
            foreach(var model in tasks){
                    TaskDTO dto = await ModelToTaskDTO(model);
                    dtos.Add(dto);
            }
            return dtos;
        }
    }
}