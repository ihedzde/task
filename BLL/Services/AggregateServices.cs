
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using BLL.Domain.Services;
using Common.DTO.Selects;
using Common.DTO.Task;

namespace BLL.Services
{
    public class AggregateServices : IAggregateServices
    {
        private IProjectService _projectService;
        private IUserService _userService;
        public AggregateServices(IProjectService projectService, IUserService userService)
        {
            _projectService = projectService;
            _userService = userService;
        }
        public async Task<Dictionary<string, int>> GetTasksEntitiesById(int userId) => userId < 0 ? throw new ArgumentException("Invalid id can't be negative") :
        (await _projectService.GetAllProjects()).Select(project => new KeyValuePair<string, int>(project.Name, project.Tasks.Count(task => task.Performer.Id == userId))).Where(x => x.Value != 0).ToDictionary(x => x.Key, x => x.Value);

        public async Task<IList<TaskDTO>> GetLessThan45(int userId) => userId < 0 ? throw new ArgumentException("Invalid id can't be negative") :
         (await _projectService.GetAllProjects()).SelectMany(project => project.Tasks).Where(task => task.PerformerId == userId && task.Name.Length < 45).ToList();

        public async Task<IList<IdAndName>> FinishedIn2021(int userId) => userId < 0 ? throw new ArgumentException("Invalid id can't be negative") :
         (await _projectService.GetAllProjects()).SelectMany(project => project.Tasks).Where(task => task.FinishedAt?.Year == 2021 && task.Performer.Id == userId)
            .Select(task => new IdAndName { Id = task.Id, Name = task.Name }).ToList();

        public async Task<IList<IGrouping<string, IdTeamNameMembers>>> TeamsOlder10Sorted() =>
        (await _projectService.GetAllProjects())
            .Select(project => new
            {
                Id = project.Team.Id,
                Name = project.Team.Name,
                CreatedYear = project.Team.CreatedAt.Year,
                Users = project.Team.Members
            }
            )
            .OrderByDescending(entity => entity.CreatedYear)
            .Select(entity => new IdTeamNameMembers
            {
                Id = entity.Id,
                TeamName = entity.Name,
                Members = entity.Users.ToList()
            })
            .GroupBy(entity => entity.TeamName).ToList();
        public async Task<IList<IdUserAndTasks>> UsersAlphabeticByTasksLength() =>
        (await _userService.GetAllUsers())
            .OrderBy(x => x.FirstName).GroupJoin(
                (await _projectService.GetAllProjects()).SelectMany(x => x.Tasks),
                user => user.Id,
                task => task.PerformerId,
                (user, task) => new IdUserAndTasks
                {
                    Id = user.Id,
                    User = user,
                    Tasks = task.OrderByDescending(t => t.Name.Length).ToList()
                }
            ).ToList();

        public async Task<UserTaskCountUnfinishedTaskCountLongestTask> ComplexUserEntity(int userId)
       => userId < 0 ? throw new ArgumentException("Invalid id can't be negative") : (await _projectService.GetAllProjects()).Where(
                project => project.AuthorId == userId
            ).OrderByDescending(x => x.CreatedAt)
            .Select(
                p => new UserTaskCountUnfinishedTaskCountLongestTask
                {
                    User = p.Author,
                    TaskCount = p.Tasks.Count(),
                    UnfinishedTaskCount = p.Tasks.Count(task => task.PerformerId == userId && task.State != TaskState.Finished && task.State != TaskState.Backlog),
                    LongestTask = p.Tasks.OrderByDescending((t) => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt).FirstOrDefault()
                }
            ).FirstOrDefault();

        public async Task<IList<ProjectTaskUserCountMix>> OddlyProjectTaskUserCountMix() =>
         (await Task.WhenAll((await _projectService.GetAllProjects())
           .Select(async project =>

             new ProjectTaskUserCountMix
             {
                 Project = project,
                 LongestDescriptionTask = project.Tasks.Count() > 2 ?
                 project.Tasks.Aggregate((t1, t2) => t1.Description.Length > t2.Description.Length ? t1 : t2)
                 : null,
                 WithShortestNameTask = project.Tasks.Count() > 2 ?
                  project.Tasks.Aggregate((t1, t2) => t1.Name.Length < t2.Name.Length ? t1 : t2)
                 : null,
                 TotalTeamUsersCount = project.Description.Length > 20 && project.Tasks.Count() < 3
                 ? (await _userService.GetAllUsers()).Where(x => x.TeamId == project.TeamId).Count()
                 : null
             }
           ))).ToList();
    }
}