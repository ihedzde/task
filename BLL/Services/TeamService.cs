using System;
using System.Linq;
using System.Collections.Generic;
using Common.DTO.Team;
using DAL.Domain.Repositories;
using AutoMapper;
using Common.DTO.User;
using DAL.Domain.Models;
using BLL.Domain.Services;
using System.Threading.Tasks;
namespace BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IRepository<TeamModel> _teamRepo;
        private readonly IRepository<UserModel> _userRepo;
        private readonly IRepository<ProjectModel> _projectRepo;
        private readonly IMapper _mapper;
        public TeamService(IMapper mapper, IRepository<TeamModel> teamRepo,
         IRepository<UserModel> userRepo, IRepository<ProjectModel> projectRepo)
        {
            _teamRepo = teamRepo;
            _userRepo = userRepo;
            _projectRepo = projectRepo;
            _mapper = mapper;
        }
        private async Task<TeamDTO> ModelToTeamDTO(TeamModel model)
        {
            IList<UserModel> members = (await _userRepo.ReadAll()).Where(user => user.TeamId == model.Id).ToList();
            return new TeamDTO
            {
                Id = model.Id,
                Name = model.Name,
                Members = members.Select(x => _mapper.Map<UserModel, UserDTO>(x)).ToList(),
                CreatedAt = model.CreatedAt
            };
        }
        private TeamModel TeamDTOToModel(TeamDTO model)
        {
            return new TeamModel
            {
                Id = model.Id,
                Name = model.Name,
                CreatedAt = model.CreatedAt
            };
        }
        private TeamDTO CreateModelToTeamDTO(CreateTeamDTO model) =>
        new TeamDTO
        {
            Name = model.Name,
            CreatedAt = DateTime.Now
        };
        public async Task<TeamDTO> GetById(int id)
        {
            return await ModelToTeamDTO(await _teamRepo.Read(id));
        }
        public async Task<IEnumerable<TeamDTO>> GetAllTeams()
        {
            var teams = (await _teamRepo.ReadAll());
            IList<TeamDTO> dtos = new List<TeamDTO>();
            foreach(var model in teams){
                    TeamDTO dto = await ModelToTeamDTO(model);
                    dtos.Add(dto);
            }
            return dtos;
        }
        public async Task<TeamDTO> CreateTeam(CreateTeamDTO createTeamDTO)
        {
            return await ModelToTeamDTO(await _teamRepo.Create(TeamDTOToModel(CreateModelToTeamDTO(createTeamDTO))));
        }
        public async Task UpdateTeam(TeamDTO updateDto)
        {
            await _teamRepo.Update(TeamDTOToModel(updateDto));
        }
        public async Task DeleteTeam(int id)
        {
            if ((await _projectRepo.ReadAll()).Any(project => project.TeamId == id))
                throw new InvalidOperationException("Cannot delete Team with dependency on Project, reasign to other teams or delete.");
            (await _userRepo.ReadAll()).Where(user => user.TeamId == id).ToList()
                    .ForEach(async user =>
                   {
                       user.TeamId = null;
                       await _userRepo.Update(user);
                   });

        }
    }
}