using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.DTO.Project;
using Common.DTO.User;
using DAL.Domain.Repositories;
using DAL.Domain.Models;
using BLL.Domain.Services;
using System.Text;
using System.Threading.Tasks;
namespace BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IRepository<ProjectModel> _projectRepo;
        private readonly IRepository<UserModel> _userRepo;
        private readonly IRepository<TeamModel> _teamRepo;
        private readonly ITaskService _taskService;
        private readonly ITeamService _teamService;
        private readonly IMapper _mapper;
        public ProjectService(IMapper mapper, IRepository<ProjectModel> projectRepo, IRepository<UserModel> userRepo,
        IRepository<TeamModel> teamRepo,
         ITaskService taskService, ITeamService teamService)
        {
            _mapper = mapper;
            _projectRepo = projectRepo;
            _userRepo = userRepo;
            _teamRepo = teamRepo;
            _taskService = taskService;
            _teamService = teamService;
        }
        private async Task<ProjectDTO> ModelToProjectDTO(ProjectModel model)
        {
            return new ProjectDTO
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                AuthorId = model.AuthorId,
                TeamId = model.TeamId,
                Author = _mapper.Map<UserDTO>(await _userRepo.Read(model.AuthorId)),
                Team = await _teamService.GetById(model.TeamId),
                Tasks = (await _taskService.GetAllTasks()).Where(task => task.PerformerId == model.Id),
                CreatedAt = model.CreatedAt,
                Deadline = model.Deadline,
            };
        }
        private ProjectModel ProjectDTOToModel(ProjectDTO model)
        {
            return new ProjectModel
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                AuthorId = model.AuthorId,
                TeamId = model.TeamId,
                CreatedAt = model.CreatedAt,
                Deadline = model.Deadline
            };
        }
        private async Task<ProjectDTO> CreateModelToProjectDTO(CreateProjectDTO model) =>
        new ProjectDTO
        {
            Name = model.Name,
            Description = model.Description,
            AuthorId = model.AuthorId,
            TeamId = model.TeamId,
            Author = _mapper.Map<UserModel, UserDTO>(await _userRepo.Read(model.AuthorId)),
            Team = await _teamService.GetById(model.TeamId),
            CreatedAt = DateTime.Now,
            Deadline = model.Deadline,
        };

        public async Task<IEnumerable<ProjectDTO>> GetAllProjects()
        {
            var projects = await _projectRepo.ReadAll();
            IList<ProjectDTO> dtos = new List<ProjectDTO>();
            foreach(var model in projects){
                    ProjectDTO dto = await ModelToProjectDTO(model);
                    dtos.Add(dto);
            }
            return dtos;
        }
        private async Task ValidateCreateDTO(CreateProjectDTO createProjectDTO)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (string.IsNullOrEmpty(createProjectDTO.Name))
                stringBuilder.AppendLine("Name can't be empty");
            if (string.IsNullOrEmpty(createProjectDTO.Description))
                stringBuilder.AppendLine("Description can't be empty");

            if (await _userRepo.Read((int)createProjectDTO.AuthorId) == null)
                stringBuilder.AppendLine($"No author with such {createProjectDTO.AuthorId} authorId was found");
            if (await _teamRepo.Read((int)createProjectDTO.TeamId) == null)
                stringBuilder.AppendLine($"No team with such {createProjectDTO.TeamId} teamId was found");

            var argumentExceptionMessage = stringBuilder.ToString();
            if (argumentExceptionMessage.Length > 0)
                throw new ArgumentException(argumentExceptionMessage);
        }
        public async Task<ProjectDTO> CreateProject(CreateProjectDTO createProjectDTO)
        {
            await ValidateCreateDTO(createProjectDTO);
            return await ModelToProjectDTO(await _projectRepo.Create(ProjectDTOToModel(await CreateModelToProjectDTO(createProjectDTO))));
        }
        public async Task<ProjectDTO> GetById(int id)
        {
            return await ModelToProjectDTO(await _projectRepo.Read(id));
        }
        public async Task UpdateProject(ProjectDTO updateDto)
        {
            await _projectRepo.Update(ProjectDTOToModel(updateDto));
        }
        public async Task DeleteProject(int id)
        {
            await _projectRepo.Delete(id);
        }

    }
}