using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.DTO.User;
using DAL.Domain.Repositories;
using DAL.Domain.Models;
using BLL.Domain.Services;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
namespace BLL.Services
{
    public  class UserService : IUserService
    {
        private readonly IRepository<UserModel> _userRepo;
        private readonly IRepository<TeamModel> _teamRepo;
        private readonly IMapper _mapper;
        public  UserService(IMapper mapper, IRepository<UserModel> userRepo, IRepository<TeamModel> teamRepo)
        {
            _userRepo = userRepo;
            _teamRepo = teamRepo;
            _mapper = mapper;
        }
        public async Task< IEnumerable<UserDTO>> GetAllUsers()
        {
            return (await _userRepo.ReadAll()).Select(model => _mapper.Map<UserModel, UserDTO>(model));
        }
        public async Task< UserDTO> GetById(int id)
        {
            return _mapper.Map<UserModel, UserDTO>(await _userRepo.Read(id));
        }
        private async Task ValidateCreateDTO(CreateUserDTO createUserDTO)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (string.IsNullOrEmpty(createUserDTO.FirstName))
                stringBuilder.AppendLine("FirstName can't be empty");
            if (string.IsNullOrEmpty(createUserDTO.LastName))
                stringBuilder.AppendLine("LastName can't be empty");
            if (createUserDTO.Email == null || !new EmailAddressAttribute().IsValid(createUserDTO.Email))
                stringBuilder.AppendLine("Email is invalid");
            if (createUserDTO.BirthDay > DateTime.Now)
                stringBuilder.AppendLine($"User birtday can't be earlier than today{DateTime.Now.ToString()}");
            if (createUserDTO.TeamId != null)
            {
                var team = await _teamRepo.Read((int)createUserDTO.TeamId);
                if (team == null)
                    stringBuilder.AppendLine($"No team with such {createUserDTO.TeamId} teamId was found");
            }

            var argumentExceptionMessage = stringBuilder.ToString();
            if (argumentExceptionMessage.Length > 0)
                throw new ArgumentException(argumentExceptionMessage);

        }
        private async Task ValidateUpdateDTO(UserDTO updateUserDTO)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (string.IsNullOrEmpty(updateUserDTO.FirstName))
                stringBuilder.AppendLine("FirstName can't be empty");
            if (string.IsNullOrEmpty(updateUserDTO.LastName))
                stringBuilder.AppendLine("LastName can't be empty");
            if (updateUserDTO.Email == null || !new EmailAddressAttribute().IsValid(updateUserDTO.Email))
                stringBuilder.AppendLine("Email is invalid");
            if (updateUserDTO.BirthDay > DateTime.Now)
                stringBuilder.AppendLine($"User birtday can't be earlier than today{DateTime.Now.ToString()}");
            if (updateUserDTO.TeamId != null)
            {
                if ((await _teamRepo.Read((int)updateUserDTO.TeamId)) == null)
                    stringBuilder.AppendLine($"No team with such {updateUserDTO.TeamId} teamId was found");
            }

            var argumentExceptionMessage = stringBuilder.ToString();
            if (argumentExceptionMessage.Length > 0)
                throw new ArgumentException(argumentExceptionMessage);

        }
        public async Task< UserDTO> CreateUser(CreateUserDTO createUserDTO)
        {
            await ValidateCreateDTO(createUserDTO);
            return _mapper.Map<UserModel, UserDTO>(await _userRepo.Create(_mapper.Map<UserDTO, UserModel>(_mapper.Map<CreateUserDTO, UserDTO>(createUserDTO))));
        }
        public async Task DeleteUser(int id)
        {
            if (id < 0)
                throw new ArgumentException("Invalid id can't be negative");
            var model = await _userRepo.Delete(id);
            if (model == null)
                throw new ArgumentException($"No such model with id {id} was found");
        }
        public async Task UpdateUser(UserDTO updateUser)
        {
            await ValidateUpdateDTO(updateUser);
            await _userRepo.Update(_mapper.Map<UserDTO, UserModel>(updateUser));
        }

    }
}