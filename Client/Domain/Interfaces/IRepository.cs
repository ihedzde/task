using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client.Domain.Interfaces
{
    public interface IRepository<T, C>
    {
        Task<T> Create(C data);
        Task<IList<T>> Read();
        Task Update(T data);
        Task Delete(int id);
    }
}