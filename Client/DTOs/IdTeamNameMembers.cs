using System.Collections.Generic;
using System.Text;
using Client.Domain.Models;

namespace Client.DTOs
{
    public struct IdTeamNameMembers
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
        public IList<UserModel> Members{ get; set; }
        
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach(var member in Members){
                builder.AppendLine(member.ToString());
            }
            
            return $"Id: {Id} Name: {TeamName}\n Members:\n {builder.ToString()}";
        }
    }
}