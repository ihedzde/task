using Client.Domain.Models;

namespace Client.DTOs
{
    public class UserTaskCountUnfinishedTaskCountLongestTask
    {
        public UserModel User { get; set; }

        public int TaskCount { get; set; }
        public int UnfinishedTaskCount { get; set; }

        public TaskModel LongestTask { get; set; }

        public override string ToString()
        {
            return $"User: {User.ToString()} TaskCount: {TaskCount} UnfinishedTaskCount: {UnfinishedTaskCount} TaskModel {LongestTask?.ToString()}";
        }
    }
}