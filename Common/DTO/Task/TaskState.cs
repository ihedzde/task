namespace Common.DTO.Task
{
    public enum TaskState: short
    {
        Development,
        Testing,
        Finished,
        Backlog
    }
}