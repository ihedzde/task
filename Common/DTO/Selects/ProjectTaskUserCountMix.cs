using Common.DTO.Project;
using Common.DTO.Task;

namespace Common.DTO.Selects
{
    public class ProjectTaskUserCountMix
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestDescriptionTask { get; set; }
        public TaskDTO WithShortestNameTask { get; set; }
        public int? TotalTeamUsersCount { get; set; }

    }
}