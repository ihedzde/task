﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BLL.Services;
using System.Threading.Tasks;
using Common.DTO.Selects;
using System.Collections.Generic;
using System.Linq;
using Common.DTO.Task;
using BLL.Domain.Services;
using System;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SelectController : ControllerBase
    {
        private readonly IAggregateServices _aggregateServices;
        public SelectController(IAggregateServices selectServices)
        {
            _aggregateServices = selectServices;
        }
        [HttpGet("0")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Dictionary<string, int>>> GetTasksEntitiesById(int id)
        {
            return Ok(await _aggregateServices.GetTasksEntitiesById(id));
        }
        [HttpGet("1")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetLessThan45(int id)
        {
            return Ok( await _aggregateServices.GetLessThan45(id));
        }
        [HttpGet("2")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<IList<IdAndName>>> FinishedIn2021(int id)
        {
            try
            {
                return Ok( await _aggregateServices.FinishedIn2021(id));
            }catch(ArgumentException e){
                return BadRequest(e.Message);
            }
        }
        [HttpGet("3")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IList<IGrouping<string, IdTeamNameMembers>>>> TeamsOlder10Sorted()
        {
            return Ok( await _aggregateServices.TeamsOlder10Sorted());
        }
        [HttpGet("4")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IList<IdUserAndTasks>>> UsersAlphabeticByTasksLength()
        {
            return Ok( await _aggregateServices.UsersAlphabeticByTasksLength());
        }
        [HttpGet("5")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<UserTaskCountUnfinishedTaskCountLongestTask>> ComplexUserEntity(int id)
        {
            return Ok(await _aggregateServices.ComplexUserEntity(id));
        }
        [HttpGet("6")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IList<ProjectTaskUserCountMix>>> OddlyProjectTaskUserCountMix()
        {
            return Ok(await _aggregateServices.OddlyProjectTaskUserCountMix());
        }
    }
}
