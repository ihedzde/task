using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using DAL.Domain.Repositories;
using DAL.Persistance.Context;
using DAL.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace DAL.Persistance.Repositories
{
    public class TaskRepository : BaseRepository, IRepository<TaskModel>
    {
        public TaskRepository(AppDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<TaskModel> Create(TaskModel data)
        {
            dbContext.Tasks.Add(data);
            await dbContext.SaveChangesAsync();
            return data;
        }

        public async Task<TaskModel> Delete(int id)
        {
            var deleteObj = dbContext.Tasks.FirstOrDefault(p => p.Id == id);
            dbContext.Tasks.Remove(deleteObj);
            await dbContext.SaveChangesAsync();
            return deleteObj;
        }

        public async Task<TaskModel> Read(int id)
        {
            return await dbContext.Tasks.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<IList<TaskModel>> ReadAll()
        {
            return await dbContext.Tasks.ToListAsync();
        }

        public async Task<TaskModel> Update(TaskModel data)
        {
            dbContext.Tasks.Update(data);
            await dbContext.SaveChangesAsync();
            return data;
        }
    }
}