using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using DAL.Domain.Repositories;
using DAL.Persistance.Context;
using DAL.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace DAL.Persistance.Repositories
{
    public class TeamRepository : BaseRepository, IRepository<TeamModel>
    {
        public TeamRepository(AppDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<TeamModel> Create(TeamModel data)
        {
            dbContext.Teams.Add(data);
            await dbContext.SaveChangesAsync();
            return data;
        }

        public async Task<TeamModel> Delete(int id)
        {
            var deleteObj = dbContext.Teams.FirstOrDefault(p => p.Id == id);
            dbContext.Teams.Remove(deleteObj);
            await dbContext.SaveChangesAsync();
            return deleteObj;
        }

        public async Task<TeamModel> Read(int id)
        {
            return await dbContext.Teams.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<IList<TeamModel>> ReadAll()
        {
            return await dbContext.Teams.ToListAsync();
        }

        public async Task<TeamModel> Update(TeamModel data)
        {
            dbContext.Teams.Update(data);
            await dbContext.SaveChangesAsync();
            return data;
        }
    }
}