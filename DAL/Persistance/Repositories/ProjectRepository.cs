using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using DAL.Domain.Repositories;
using DAL.Persistance.Context;
using DAL.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace DAL.Persistance.Repositories
{
    public class ProjectRepository : BaseRepository, IRepository<ProjectModel>
    {
        public ProjectRepository(AppDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<ProjectModel> Create(ProjectModel data)
        {
            dbContext.Projects.Add(data);
            await dbContext.SaveChangesAsync();
            return data;
        }

        public async Task<ProjectModel> Delete(int id)
        {
            var deleteObj = dbContext.Projects.FirstOrDefault(p => p.Id == id);
            dbContext.Projects.Remove(deleteObj);
            await dbContext.SaveChangesAsync();
            return deleteObj;
        }

        public async Task<ProjectModel> Read(int id)
        {
            return await dbContext.Projects.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<IList<ProjectModel>> ReadAll()
        {
            return await dbContext.Projects.ToListAsync();
        }

        public async Task<ProjectModel> Update(ProjectModel data)
        {
            dbContext.Projects.Update(data);
            await dbContext.SaveChangesAsync();
            return data;
        }
    }
}