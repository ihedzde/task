using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using DAL.Domain.Repositories;
using DAL.Persistance.Context;
using DAL.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace DAL.Persistance.Repositories
{
    public class UserRepository : BaseRepository, IRepository<UserModel>
    {
        public UserRepository(AppDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<UserModel> Create(UserModel data)
        {
            dbContext.Users.Add(data);
            await dbContext.SaveChangesAsync();
            return data;
        }

        public async Task<UserModel> Delete(int id)
        {
            var deleteObj = dbContext.Users.FirstOrDefault(p => p.Id == id);
            dbContext.Users.Remove(deleteObj);
            await dbContext.SaveChangesAsync();
            return deleteObj;
        }

        public async Task<UserModel> Read(int id)
        {
            return await dbContext.Users.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<IList<UserModel>> ReadAll()
        {
            return await dbContext.Users.ToListAsync();
        }

        public async Task<UserModel> Update(UserModel data)
        {
            dbContext.Users.Update(data);
            await dbContext.SaveChangesAsync();
            return data;
        }
    }
}