using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace WebAPI.IntegrationTests
{
    public class TaskControllerTests : IClassFixture<WebAPIFactory<Startup>>
    {
        private readonly WebAPIFactory<Startup> _factory;
        private readonly HttpClient _client;
        public TaskControllerTests(WebAPIFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false,
                BaseAddress = new Uri("https://localhost:5001")
            });
        }
        [Fact]
        public async Task Get_GetAllUnfinishedForUser_WhenValidUserId_ThanReturnsOk()
        {
            //Arrange
            int userId = 10;
            //Act
            var response = await _client.GetAsync($"/api/task/unfinished/{userId}");
            //Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
        [Fact]
        public async Task Get_GetAllUnfinishedForUser_WhenInvalidUserId_ThanReturnsBadRequest()
        {
            //Arrange
            int userId = -121;
            //Act
            var response = await _client.GetAsync($"/api/task/unfinished/{userId}");
            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
        [Fact]
        public async Task Delete_DeleteTask_WhenValidId_ThanReturnsNoContent()
        {
            //Arrange
            int taskId = 2;
            //Act
            var response = await _client.DeleteAsync($"/api/task?id={taskId}");
            //Assert
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }
        [Fact]
        public async Task Delete_DeleteTask_WhenInvalidId_ThanReturnsBadRequest()
        {
            //Arrange
            int taskId = 2002;
            //Act
            var response = await _client.DeleteAsync($"/api/task?id={taskId}");
            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}